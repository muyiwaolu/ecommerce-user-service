<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Auth;


class UsersController extends Controller
{

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'api_token' => str_random(32)
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
        ]);

        $user = $this->create($request->all());
        $user->api_token = str_random(32);
        return response()->json([
            'ok' => true,
            'data' => $user->api_token
        ]);
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    public function login(Request $request)
    {
        $this->validate($request, ['email' => 'required|email|max:255']);

        # More verification would be used in the real world.
        if (User::where('email', $request->email)->exists()) {
            $user = User::where('email', $request->email)->first();
            return response()->json(['ok' => true, 'data' => $user->api_token]);
        }
        return response()->json(['ok' => false, 'message' => 'The details provided are invalid.']);
    }
}
