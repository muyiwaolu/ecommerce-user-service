<?php

use Laravel\Lumen\Testing\DatabaseMigrations;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function can_register_users()
    {
        $this->json('POST', '/register', ['name' => 'Muyiwa',
            'email' => 'test@muyiwa.com',
        ])
        ->seeJson(['ok' => true]);
    }

    /**
     * @test
     */
    public function can_login_users()
    {
        $user = factory(App\User::class)->create();
        $this->json('POST', '/login', ['email' => $user->email])
            ->seeJson(['ok' => true]);
    }
}
